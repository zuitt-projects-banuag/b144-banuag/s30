const express = require("express");
//Mongooes is a package that allows creation of schemas to model our data structures and to have an access to a number of methods for manipulating our database.
const mongoose = require("mongoose");

const app = express();
const port = 3001;

//MongoDB Connection
//Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://user:user123@cluster0.ok2rk.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

//Set notification for connection success or failure
let db = mongoose.connection;
//If error occured, output in the console
db.on("error", console.error.bind(console, "connection error"))
//If the connection is successfull, output in the console
db.once("open", () => console.log("We're connected to the cloud database."))

app.use(express.json());
app.use(express.urlencoded({extend: true}))

//Mongoose Schemas
//Schemas determine the structure of the documents to be written in the database.
//It acts as a blueprints to our data
//We will use Schema() constructor of the Mongoose module to create a new Schema object
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		//Default values are the predefined values for a field If we don't put any values
		default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema)
//Models are what allows us to gain access to methods that will perform CRUD functions.
//Models must be in singular form and capitalized following the MVC approach for naming conventions
//first parameter of the mongoose model method indicates the collection in where to store the data
//second parameter is used to specify the Schema/blueprint
//----------------------------------------------
//Routes
//Create a new task
/*
	Business Logic
	1. Add a functionality to check if there are 	duplicates tasks
		-if the task already exists, return there is a duplicate
		-if the task doesn't exist, we can add it in the database
	2. The task data will be coming 	from the request's body
	3. Create new Task object with 		properties that we need
	4. Then save the data
*/

app.post("/tasks", (req, res) => {
	//Error-Handling
	//Check if there are duplicate tasks
	//findOne() is a mongoose method that acts similar to "find"
	//It returns the first document that matches the search criteria
	Task.findOne( {name: req.body.name}, (error, result) => {
			//If a document was found and the document's name matches the information sent via client/postman
			if(result !== null && result.name == req.body.name){
					//Return a message to the client/postman
					return res.send("Duplicate task found")
			}else{
				//If no document was found
				//Create a new task and save it to the database
				let newTask = new Task({
					name: req.body.name
				})
				//It will save the information
				newTask.save((saveErr, savedTask) => {
					//If there are errors in saving
					//saveErr is an error object that will contain details about the error
					//Errors normally come as an object data type
					if(saveErr){
						return console.error(saveErr)
					}else{
						//no error found while creating the document
						//Return as status code of 201 for successful creation
						return res.status(201).send("New Task Created")
					}
				})
			}
	} )
})

/*
	Retrieving all data
	Business Logic
	1. Retrieve all the documents using find()
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client and return an array of documents
*/

app.get("/tasks", (req, res) => {
	//An empty "{}" means it returns all the documents and stores them in the "result" parameter of the call back function
	Task.find( {}, (err, result) => {
		//If an error occured
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
})

/*
	Register a user
	Business Logic
	1. Find if there are duplicate user
		- If user already exists, we return an error
		- If the user doesn't exist, we add it in the database
		 - If the username and password are both not blank
		 	-If blank, send response "BOTH username and password must be provided"
		 	- Both condition has been met, create a new object.
		 	- Save the new object
		 		- if error, return an error message
		 		- else, response a status 201 and "New User Registered"
*/


//User Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String,
	status: {
		type: String
	}
})

//User Model
const User = mongoose.model("User", userSchema)

app.post("/signup", (req, res) => {
	User.findOne( {username: req.body.username, password: req.body.password}, (error, result) => {
			
			if((result !== null && result.username == req.body.username) || (result !== null && result.username == req.body.username)) {
					return res.send("BOTH username and password must be provided")
			}else{
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				})
				 
				newUser.save((saveErr, savedTask) => {
			
					if(saveErr){
						return console.error(saveErr)
					}else{	
						return res.status(201).send("New User Registered")
					}
				})
			}
	} )
})

app.get("/users", (req, res) => {

	User.find( {}, (err, result) => {
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port${port}`))
